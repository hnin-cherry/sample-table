<?php
	require_once('connection.php');

	if($conn){
		$id = $_POST['id'];
        $member_name = $_POST['member_name'];
        $age = $_POST['age'];
		
		$update_sql = "update tb_sample set name=? , age = ? where id = ?";
		$stmt = $conn->prepare($update_sql);
		$stmt->bind_param("sii",$member_name,$age,$id);
		$stmt->execute();

		if($stmt){
			header("location:member_layout.php");
			$stmt->close();
			$conn->close();
		}else{
            echo "update error";
        }
	
	}
?>