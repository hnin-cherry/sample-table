<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Create new member</h2>
  <a href="member_layout.php" style="float: right;"><button class="btn btn-primary">Member list</button></a>
  <form style="margin-top: 50px" action="add_member_process.php" method="post">
    <div class="form-group">
      <label for="member_name">Name:</label>
      <input type="text" class="form-control" id="member_name" placeholder="Enter name" name="member_name" required>
    </div>
    <div class="form-group">
      <label for="age">Age:</label>
      <input type="number" class="form-control" id="age" placeholder="Enter age" name="age" required>
    </div>
    <button type="submit" class="btn btn-primary">Add member</button>
  </form>
</div>

</body>
</html>
