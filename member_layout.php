<?php
require_once('connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>

<body>

  <div class="container">
    <h2>Member Table</h2>
    <a href="member_add_layout.php"><button class="btn btn-primary">Create new member</button></a>
    <table class="table" style="margin-top:20px">
      <thead>
        <tr>
          <th>Name</th>
          <th>Age</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $select_sql = "select * from tb_sample";
        $stmt = $conn->prepare($select_sql);
        $stmt->execute();
        $stmt->bind_result($id, $name, $age);
        $deptArray = array();
        while ($stmt->fetch()) {
        ?>
          <tr>
            <td><?php echo $name ?></td>
            <td><?php echo $age ?></td>
            <td>
              <!--icon source from => https://fontawesome.com/-->
              <button class="btn btn-primary" data-toggle="modal" data-target="#editModal<?php echo $id ?>"><i class="fas fa-edit"></i></button>
              <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal<?php echo $id ?>"><i class="fas fa-trash-alt"></i></button>
            </td>
          </tr>

          <!-- The Modal for delete -->
          <div class="modal" id="deleteModal<?php echo $id ?>">
            <div class="modal-dialog">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">Delete</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                  Confirm to delete
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <a class="btn btn-primary" href="delete_process.php?id=<?php echo $id ?>">Ok</a>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>

          <!-- The Modal for edit -->
          <div class="modal" id="editModal<?php echo $id ?>">
            <div class="modal-dialog">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">Edit</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                  <form action="update_process.php" method="post">
                    <div class="form-group">
                      <label for="member_name">Name:</label>
                      <input type="text" class="form-control" value="<?php echo $name ?>" name="member_name" required>
                    </div>
                    <div class="form-group">
                      <label for="age">Age:</label>
                      <input type="number" class="form-control" value="<?php echo $age ?>" name="age" required>
                    </div>
                    <input type="hidden" value="<?php echo $id ?>" name="id"/>
                    <button type="submit" style="float: right;margin-top:10px;margin-left:10px" class="btn btn-primary">Ok</button>
                    <button type="button" style="float: right;margin-top:10px;" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>

        <?php
        }
        $stmt->close();
        ?>
      </tbody>
    </table>
  </div>

</body>

</html>